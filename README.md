android-app
===========

# **哎嘛 Android 客户端项目简析** #

*注：本文假设你已经有Android开发环境*

启动Eclipse，点击菜单并导入Android客户端项目，请确保你当前的Android SDK是最新版。<br>
如果编译出错，请修改项目根目录下的 project.properties 文件。<br>
推荐使用Android 4.0 以上版本的SDK,请使用JDK1.6编译：<br>

本项目使用了一些开源库，你可以在附件中下使用他们。

> target=android-15

**本项目采用 GPL 授权协议，欢迎大家在这个基础上进行改进，并与大家分享。**

**应用截图**<br>
1-资讯页<br>
<img src="http://git.oschina.net/tonlin/android-app/raw/master/screenshots/1-%E8%B5%84%E8%AE%AF%E9%A1%B5.png"/>